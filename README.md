# Mini project

## Requirements

1. Java 8 (or newer), any implementation
2. Maven 3.2.1 (or newer)
3. Docker 18.06.1 (or newer)
    
## Prerequisites

Docker image of postgres
    ```bash
    $ docker run --name pg-dockers -e POSTGRES_PASSWORD=miniproj -e POSTGRES_DB=miniproj -d -p 5432:5432 postgres
    ```

## Usage

### Valid Requests
Some requests require header data for simple authentication.
    ``` 
    Header:
    ``` 
    ```username : {username},
    token : {token}
    ```


#### Channel resource

1. Retrieve all channels. Supports paging, sorting and filtering
    `` GET localhost:8080/channel ``

2. Create new channel, requires authentication header data
    `` POST  localhost:8080/channel``
    `` Body:  { "name" : "channelname" }``

3. Remove the channel {channelName}, requires authentication header data
    `` DELETE  localhost:8080/channel/{channelName}``

4. Subscribe to channel {channelName}, requires authentication header data
    `` POST  localhost:8080/channel/{channelName}/subscribe``
    
5. Unsubscribe from channel {channelName}, requires authentication header data
    `` POST  localhost:8080/channel/{channelName}/unsubscribe``


#### News resource

1. Retrieve all news for current user, requires authentication header data
    `` GET  localhost:8080/news``

2. Retrieve all news for current user from channel {channelName}, requires authentication header data
    `` GET  localhost:8080/news/{channelName}``

3. Post news to channel {channelName}, requires authentication header data
    `` POST  localhost:8080/channel/{channelName}/subscribe``
    `` Body:  { "title" : "newsTitle", "content" : "newsContent" }``
