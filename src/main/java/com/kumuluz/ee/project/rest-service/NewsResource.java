package com.kumuluz.ee.project;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.utils.JPAUtils;

import java.util.List;

@ApplicationScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("news")
public class NewsResource{

    @Context
    protected UriInfo uriInfo;

    @Inject
    private NewsService newsBean;

    @GET
    public Response getNews(@HeaderParam("username") String username, @HeaderParam("token") String token) {  
        QueryParameters query = QueryParameters.query(uriInfo.getRequestUri().getQuery()).build();
        User user = new User();
        user.setToken(token);
        user.setUsername(username);
        List<News> news = newsBean.getNews(user);

        return Response.ok(news).build();
    }

    @GET
    @Path("{channelName}")
    public Response getNewsFromChannel(@PathParam("channelName") String channelName, @HeaderParam("username") String username, @HeaderParam("token") String token) {
        User user = new User();
        user.setToken(token);
        user.setUsername(username);
        List<News> news = newsBean.getChannelNews(channelName, user);
        
        return Response.ok(news).build();
    }

    @POST
    @Path("{channelName}")
    public Response postToChannel(@PathParam("channelName") String channelName, @HeaderParam("username") String username, @HeaderParam("token") String token, News news) {
        User user = new User();
        user.setToken(token);
        user.setUsername(username);
        newsBean.postChannelNews(channelName, news, user);
        
        return Response.noContent().build();
    }
}