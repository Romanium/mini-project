package com.kumuluz.ee.project;

import javax.validation.constraints.*;
import javax.persistence.*;
import java.io.Serializable;

import java.util.List;

@Entity
@Table(name = "users")
@NamedQueries({
        @NamedQuery(
                name = "User.findUsers",
                query = "SELECT u " +
                        "FROM User u"
        ),
        @NamedQuery(
                name = "User.joinSubscription",
                query = "SELECT u,s " +
                        "FROM User u, Subscription s WHERE u.id = s.subscriber"
        ),
        @NamedQuery(
                name = "User.findByNameAndToken",
                query = "SELECT u " +
                        "FROM User u WHERE u.username=:username AND u.token=:token"
        )
})

public class User implements Serializable{

    @Id
    @GeneratedValue( strategy=GenerationType.AUTO )
    private int id;
    
    @Column(name = "username")
    @NotNull
    @Size(min = 1, max = 30)
    private String username;

    @Column(name = "token")
    @NotNull
    @Size(min = 1, max = 30)
    private String token;

    @OneToMany(mappedBy="userBind")
    private List<News> newsBind;

    @OneToMany(mappedBy="userBind")
    private List<Channel> channelBind;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "User {\n" +
               "  id='"       + id        + "',\n" +
               "  username='" + username  + "',\n" +
               "  token='"    + token     + "'\n"  +
               "}";
    }
}
