package com.kumuluz.ee.project;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.transaction.UserTransaction;
import java.util.List;

@RequestScoped
public class NewsService {

    @PersistenceContext
    private EntityManager em;

    public News getNews(int newsId) {
        return em.find(News.class, newsId);
    }

    public List<News> getNews(User user) {
        List<News> news = em
                .createNamedQuery("News.findNews", News.class)
                .getResultList();

        return news;
    }

    public List<News> getChannelNews(String channelName, User user) {
        List<News> news = em
                .createNamedQuery("News.findChannelNews", News.class)
                .setParameter("username", user.getUsername())
                .setParameter("token", user.getToken())
                .setParameter("channelName", channelName)
                .getResultList();

        return news;
    }

    @Transactional
    public void postChannelNews(String channelName, News news, User user) {
        Channel channel = em.createNamedQuery("Channel.findByName", Channel.class)
                .setParameter("name", channelName)
                .getSingleResult();

        User userRet = em.createNamedQuery("User.findByNameAndToken", User.class)
                .setParameter("username", user.getUsername())
                .setParameter("token", user.getToken())
                .getSingleResult();
                
        if (news != null) {
            news.setOwner(userRet.getId());
            news.setChannel(channel.getId());
            em.persist(news);
        }
    }

    @Transactional
    public void saveNews(News news) {
        if (news != null) {
            em.persist(news);
        }
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public void deleteNews(int newsId) {
        News news = em.find(News.class, newsId);
        if (news != null) {
            em.remove(news);
        }
    }
}