package com.kumuluz.ee.project;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import com.kumuluz.ee.rest.beans.QueryParameters;

import java.util.List;

@ApplicationScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("channel")
public class ChannelResource{

    @Context
    protected UriInfo uriInfo;

    @Inject
    private ChannelService channelBean;

    @GET
    public Response getChannels() {
        QueryParameters query = QueryParameters.query(uriInfo.getRequestUri().getQuery()).build();
        
        List<Channel> lch = channelBean.getChannels(query);
        long lchCount = channelBean.getChannelCount(query);

        return Response.ok(lch).header("X-Total-Count", lchCount).build();
    }

    @POST
    public Response createChannel(@HeaderParam("username") String username, @HeaderParam("token") String token, Channel channel) {
        User user = new User();
        user.setToken(token);
        user.setUsername(username);
        channelBean.saveChannel(channel, user);

        return Response.noContent().build();
    }

    @DELETE
    @Path("{channelName}")
    public Response removeChannel(@PathParam("channelName") String channelName, @HeaderParam("username") String username, @HeaderParam("token") String token) { 
        User user = new User();
        user.setToken(token);
        user.setUsername(username);
        channelBean.deleteChannel(channelName, user);
        
        return Response.noContent().build();
    }

    @POST
    @Path("{channelName}/subscribe")
    public Response subscribeToChannel(@PathParam("channelName") String channelName, @HeaderParam("username") String username, @HeaderParam("token") String token) { 
        User user = new User();
        user.setToken(token);
        user.setUsername(username);
        channelBean.subscribeToChannel(channelName, user);

        return Response.noContent().build();
    }

    @POST
    @Path("{channelName}/unsubscribe")
    public Response unsubscribeFromChannel(@PathParam("channelName") String channelName, @HeaderParam("username") String username, @HeaderParam("token") String token) { 
        User user = new User();
        user.setToken(token);
        user.setUsername(username);
        channelBean.unsubscribeFromChannel(channelName, user);

        return Response.noContent().build();
    }
}