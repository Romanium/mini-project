package com.kumuluz.ee.project;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.transaction.UserTransaction;

import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.utils.JPAUtils;

import java.util.List;

@ApplicationScoped
public class ChannelService {

    @PersistenceContext
    private EntityManager em;

    public Channel getChannel(int channelId) {
        return em.find(Channel.class, channelId);
    }

    public List<Channel> getChannels(QueryParameters query) {
        return JPAUtils.queryEntities(em, Channel.class, query);
    }

    public long getChannelCount(QueryParameters query) {
        return JPAUtils.queryEntitiesCount(em, Channel.class, query);
    }

    public List<Channel> getActiveChannels() {
        List<Channel> channels = em
                .createNamedQuery("Channel.findActiveChannels", Channel.class)
                .getResultList();

        return channels;
    }

    @Transactional
    public void saveChannel(Channel channel, User user) {
        Channel channelRet = em.createNamedQuery("Channel.findByName", Channel.class)
                .setParameter("name", channel.getName())
                .getSingleResult();

        User userRet = em.createNamedQuery("User.findByNameAndToken", User.class)
                .setParameter("username", user.getUsername())
                .setParameter("token", user.getToken())
                .getSingleResult();

        if (userRet != null) {
            if (channelRet != null && !channelRet.getActive()){
                channelRet.setOwner(userRet.getId());
                channelRet.setActive(true);
                em.persist(channelRet);
            } else {
                channel.setOwner(userRet.getId());
                em.persist(channel);
            }
        } 
    }

    @Transactional
    public void subscribeToChannel(String channelName, User user) {
        Channel channel = em.createNamedQuery("Channel.findByName", Channel.class)
                .setParameter("name", channelName)
                .getSingleResult();

        User userRet = em.createNamedQuery("User.findByNameAndToken", User.class)
                .setParameter("username", user.getUsername())
                .setParameter("token", user.getToken())
                .getSingleResult();

        if (channel != null && userRet != null) {
            Subscription subscription = em.find(Subscription.class, new Subscription.SubscriptionKey(userRet.getId(), channel.getId()));
            if (subscription != null){
                subscription.setActive(true);
            } else {
                subscription = new Subscription();
                subscription.setSubscriber(userRet.getId());
                subscription.setChannel(channel.getId());
                subscription.setLastNews(0);
            }
            em.persist(subscription);
        }
    }

    @Transactional
    public void unsubscribeFromChannel(String channelName, User user) {
        Channel channel = em.createNamedQuery("Channel.findByName", Channel.class)
                .setParameter("name", channelName)
                .getSingleResult();

        User userRet = em.createNamedQuery("User.findByNameAndToken", User.class)
                .setParameter("username", user.getUsername())
                .setParameter("token", user.getToken())
                .getSingleResult();

        if (channel != null && userRet != null) {
            Subscription subscription = em.find(Subscription.class, new Subscription.SubscriptionKey(userRet.getId(), channel.getId()));
            if (subscription != null){
                subscription.setActive(false);
                em.persist(subscription);
            }
        }
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public void deleteChannel(int channelId) {
        Channel channel = em.find(Channel.class, channelId);
        if (channel != null) {
            em.remove(channel);
        }
    }

    @Transactional
    public void deleteChannel(String channelName, User user) {
        Channel channel = em.createNamedQuery("Channel.findByUser", Channel.class)
                .setParameter("name", channelName)
                .setParameter("username", user.getUsername())
                .setParameter("token", user.getToken())
                .getSingleResult();
        if (channel != null) {
            channel.setActive(false);
            em.persist(channel);
        }
    }
}