package com.kumuluz.ee.project;

import javax.validation.constraints.*;
import javax.persistence.*;
import java.io.Serializable;

import java.util.List;

@Entity
@Table(name = "channels")
@NamedQueries({
        @NamedQuery(
                name = "Channel.findChannels",
                query = "SELECT c " +
                        "FROM Channel c"
        ),
        @NamedQuery(
                name = "Channel.findActiveChannels",
                query = "SELECT c " +
                        "FROM Channel c WHERE c.active=TRUE"
        ),
        @NamedQuery(
                name = "Channel.findByName",
                query = "SELECT c " +
                        "FROM Channel c WHERE c.name=:name"
        ),
        @NamedQuery(
                name = "Channel.findByUser",
                query = "SELECT c " +
                        "FROM Channel c, User u WHERE c.name=:name AND u.username=:username AND u.token=:token AND c.owner=u.id"
        ),
        @NamedQuery(
                name = "Channel.joinSubscription",
                query = "SELECT c,s " +
                        "FROM Channel c, Subscription s WHERE c.id = s.channel"
        )
})

public class Channel implements Serializable{

    @Id
    @GeneratedValue( strategy=GenerationType.AUTO )
    private int id;
    
    @Column(name = "name")
    @NotNull
    @Size(min = 1, max = 30)
    private String name;

    @Column(name = "owner")
    @NotNull
    private int owner;

    @NotNull
    private boolean active = true;

    @OneToMany(mappedBy="channelBind")
    private List<News> newsBind;

    @ManyToOne
    @PrimaryKeyJoinColumn(name="owner")
    private User userBind;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Channel {\n" +
               "  id='"     + id      + "',\n" +
               "  name='"   + name    + "',\n" +
               "  owner='"  + owner   + "'\n"  +
               "}";
    }
}
