package com.kumuluz.ee.project;

import javax.validation.constraints.*;
import javax.persistence.*;
import java.io.Serializable;

import java.util.List;

@Entity
@Table(name = "subscriptions")
@NamedQueries({
        @NamedQuery(
                name = "Subscription.findSubscriptions",
                query = "SELECT s " +
                        "FROM Subscription s"
        ),
        @NamedQuery(
                name = "Subscription.joinUser",
                query = "SELECT s,u " +
                        "FROM Subscription s, User u WHERE u.id = s.subscriber"
        ),
        @NamedQuery(
                name = "Subscription.joinChannel",
                query = "SELECT s,c " +
                        "FROM Subscription s, Channel c WHERE c.id = s.channel"
        )
})

@IdClass(Subscription.SubscriptionKey.class)
public class Subscription implements Serializable{

    static class SubscriptionKey implements Serializable {
        private int subscriber;
        private int channel;

        public SubscriptionKey(int subscriber, int channel){
            this.subscriber = subscriber;
            this.channel = channel;
        }

        public void setSubscriber(int subscriber){
            this.subscriber=subscriber;
        }

        public void setChannel(int channel){
            this.channel=channel;
        }
    }

    @Id
    private int subscriber, channel;

    @Column(name = "last_news")
    @NotNull
    private int lastNews;

    @NotNull
    private boolean active = true;

    public int getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(int subscriber) {
        this.subscriber = subscriber;
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }

    public int getLastNews() {
        return lastNews;
    }

    public void setLastNews(int lastNews) {
        this.lastNews = lastNews;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Subscription {\n" +
               "  subscriber='" + subscriber + "',\n" +
               "  channel='"    + channel    + "',\n"  +
               "  lastNews='"   + lastNews   + "'\n"  +
               "}";
    }
}
