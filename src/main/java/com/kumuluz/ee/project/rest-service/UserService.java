package com.kumuluz.ee.project;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.transaction.UserTransaction;
import java.util.List;

@RequestScoped
public class UserService {

    @PersistenceContext
    private EntityManager em;

    public News getNews(int newsId) {
        return em.find(News.class, newsId);
    }

    public List<News> getNews() {
        List<News> news = em
                .createNamedQuery("News.findNews", News.class)
                .getResultList();

        return news;
    }

    @Transactional
    public void saveNews(News news) {
        if (news != null) {
            em.persist(news);
        }

    }

    @Transactional(Transactional.TxType.REQUIRED)
    public void deleteNews(int newsId) {
        News news = em.find(News.class, newsId);
        if (news != null) {
            em.remove(news);
        }
    }
}