package com.kumuluz.ee.project;

import javax.validation.constraints.*;
import javax.persistence.*;
import java.io.Serializable;

import java.util.List;

@Entity
@Table(name = "news")
@NamedQueries({
        @NamedQuery(
                name = "News.findNews",
                query = "SELECT n " +
                        "FROM News n"
        ),
        @NamedQuery(
                name = "News.findNewsByUser",
                query = "SELECT n,s " +
                        "FROM News n, User u, Subscription s " +
                        "WHERE u.username=:username AND u.token=:token AND u.id=s.subscriber AND s.channel=n.channel AND n.id>s.lastNews"
        ),
        @NamedQuery(
                name = "News.findChannelNews",
                query = "SELECT n " +
                        "FROM News n, User u, Subscription s, Channel c " +
                        "WHERE u.username=:username AND u.token=:token AND u.id=s.subscriber AND s.active=TRUE " +
                        "AND c.name=:channelName AND c.active=TRUE AND c.id=s.channel AND s.channel=n.channel AND n.id>s.lastNews"
        )
})

public class News implements Serializable{

    @Id
    @GeneratedValue( strategy=GenerationType.AUTO )
    private int id;

    @Column(name = "title")
    @NotNull
    @Size(min = 1, max = 255)
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "owner")
    @NotNull
    private int owner;

    @Column(name = "channel")
    @NotNull
    private int channel;

    @ManyToOne
    @PrimaryKeyJoinColumn(name="owner")
    private User userBind;

    @ManyToOne
    @PrimaryKeyJoinColumn(name="channel")
    private Channel channelBind;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }

    @Override
    public String toString() {
        return "News {\n" +
               "  id='"      + id      + "',\n" +
               "  title='"   + title   + "',\n" +
               "  content='" + content + "',\n" +
               "  owner='"   + owner   + "',\n" +
               "  channel='" + channel + "'\n"  +
               "}";
    }
}
